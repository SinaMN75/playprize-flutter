part of 'core.dart';

class AppConstants {}

class AppColors {}

class AppThemes {
  static ThemeData defaultTheme = ThemeData();
}

class AppTextStyles {
  static const TextStyle heading1 = TextStyle();
}

class AppImages {
  static const String _base = "assets/images/";
}

class AppIcons {
  static const String _base = "assets/icons/";
}
