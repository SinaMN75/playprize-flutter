import 'package:flutter/material.dart';
import 'package:playprize_flutter/core/core.dart';
import 'package:utilities/utilities.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetMaterialApp(
        title: 'Play Prize',
        theme: AppThemes.defaultTheme,
        home: Container(),
      );
}
